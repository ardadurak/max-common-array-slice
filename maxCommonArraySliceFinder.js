/**
 * maxCommonArraySliceFinder.js
 *
 * @description
 * Solution for Max Common Array Slice problem (Test1.pdf)
 * @author  Arda Durak arda.durak@gmail.com
 * @updated 2017-02-12
 *
 */
 var input_stdin = '';

 process.stdin.resume();
 process.stdin.setEncoding('ascii');
 process.stdin.on('data', function (data) {
   if (data === 'exit\n' || data === 'close\n' || data === 'finish\n'
  || data === 'quit\n' || data === 'end\n' || data === 'stop\n') {
     process.exit(0);
   }
   main(data);
 });

 function main(input) {
 // remove any white space and the square brackets - just in case
   var array = (input.replace(/\s+/g, '').replace(/[\[\]']+/g, '')).split(',');
   var invalidElements = false;

   array = array.map(function (value) {
     if (!value || isNaN(value)) {
       invalidElements = true;
       return -1;
     }
     return Number(value);
   });

   if (invalidElements) {
     console.log('There are invalid elements in the array');
   } else {
     findSlice(array);
   }
 }

 function findSlice(array) {
 // initialize all arrays
   var currentArray = [];
   var identicalArray = [];
   var maxArray = [];

   if (array.length < 3) {
     maxArray = array;
   }
   else {
     currentArray.push(array[0]);

  // Keep adding identical elements
     var loopValue;
     for (var i = 1; i < array.length; i++) {
       loopValue = i;
       currentArray.push(array[i]);
       if (array[i] !== array[0]) { // loop until the first different value
         break;
       }
     }

     maxArray = currentArray; // n (>= 1) identical numbers and one different number x
     identicalArray.push(array[loopValue]); // first element of the array is x

     for (var i = loopValue + 1; i < array.length; i++) {
       if (currentArray.indexOf(array[i]) === -1) {    // if the element doesn't exist already
         if (currentArray.length > maxArray.length) { // compare current array with max array
           maxArray = currentArray;
         }
         currentArray = identicalArray;
       }
       currentArray.push(array[i]); // first different element of the new array
       if (array[i] !== identicalArray[0]) {
         identicalArray = []; // initialize the identical array if the current value is different
       }
       identicalArray.push(array[i]);
     }
   }

  // final check for the max array
   if (currentArray.length > maxArray.length) {
     maxArray = currentArray;
   }
   console.log(maxArray.length);
 }
