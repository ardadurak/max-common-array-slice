Max Common Array Slice Finder
===============================

A program which takes a comma delimited array of numbers from STDIN and will output the maximum slice of the array which contains no more than two different numbers.
The result is written to STDOUT.
